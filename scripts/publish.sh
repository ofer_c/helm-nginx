if helm history --max 1 my-cherry-chart 2>/dev/null | grep FAILED | cut -f1 | grep -q 1; then
    echo "helm purging..."
    helm delete --purge my-cherry-chart
fi

helm install my-cherry-chart ./ --values ./values.yaml

echo "Installation successfull, pushing to registry:"
# helm registry push ${REGISTRY}/${REGISTRY_USER}/${CHART_NAME}

