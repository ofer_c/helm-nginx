# helm-nginx
Run local nginx server with helm and minikube

# Prerequisites

* Install minikube
* Install helm
* Start minikube - make sure kubernates version is max 1.15.4 ( as latest is not supported with helm )
You can do it with ```minikube start --kubernetes-version=1.15.4```

# Setup instructions
* Clone the repository
* Initialize helm with 
```
helm init
```
* Install app with
```
helm install --name my-cherry-chart helm-nginx/ --values helm-nginx/values.yaml  
```
Then installation instructions will be displayed in terminalcop, for example: 
```
export POD_NAME=$(kubectl get pods -l "app.kubernetes.io/name=buildachart,app.kubernetes.io/instance=my-cherry-chart" -o jsonpath="{.items[0].metadata.name}")
export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services cherry-chart)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
echo http://$NODE_IP:$NODE_PORT
```
Follow and copy paste lines in terminal

Visit http://$NODE_IP:$NODE_PORT


# Runner Setup instructions

* Install minikube
* Install helm
* Start minikube (e.g "minikube start --vm-driver=virtualbox")
* helm repo add gitlab https://charts.gitlab.io
* helm repo update
* wget https://gitlab.com/gitlab-org/charts/gitlab-runner/-/raw/master/values.yaml
* Fill values yaml with registration token
```yaml
gitlabUrl: https://gitlab.com/
runnerRegistrationToke_n: "WykRyfDg95BGMVAzjGhZ"
rbac:
  create: true
  [..]
  serviceAccountName: default
```
* kubectl create ns gitlab-runners
* Create a role:
```
$ cat <<EOF | kubectl create -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: gitlab-runner
  namespace: gitlab-runners
rules:
  - apiGroups: [""]
    resources: ["pods","services","secrets"]
    verbs: ["list", "get", "watch", "create", "delete"]
  - apiGroups: ["apps"]
    resources: ["deployments"]
    verbs: ["list", "get", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["pods/exec"]
    verbs: ["create"]
  - apiGroups: [""]
    resources: ["pods/log"]
    verbs: ["get"]
EOF
$ kubectl create rolebinding --namespace=gitlab-runners gitlab-runner-binding --role=gitlab-runner --serviceaccount=gitlab-runners:default
```
* Install the runner:
helm install --namespace gitlab-runners gitlab-runner -f ./values.yaml gitlab/gitlab-runner
or, for 2nd time and up:

helm upgrade --install --namespace gitlab-runners gitlab-runner -f ./values.yaml gitlab/gitlab-runner



